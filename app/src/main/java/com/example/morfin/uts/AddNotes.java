package com.example.morfin.uts;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class AddNotes extends AppCompatActivity {
    EditText editTextNotes;
    String nim;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_notes);
        if (!SharedPrefManager.getInstance(this).isLoggedIn()) {
            finish();
            startActivity(new Intent(this, LoginActivity.class));
        };
        nim = SharedPrefManager.getInstance(this).getUser().getNim().toString().trim();
        editTextNotes = (EditText) findViewById(R.id.editTextNotes);
        findViewById(R.id.buttonSubmit).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                submit();
            }
        });
    }


    public boolean onCreateOptionsMenu(Menu menu) {
        return true;
    }
    private void submit() {
        //first getting the values
        final String notes = editTextNotes.getText().toString().trim();

        //validating inputs
        if(notes.isEmpty()){
            editTextNotes.setError("Harus diisi");
        }else{
            //if everything is fine
            StringRequest stringRequest = new StringRequest(Request.Method.POST, URLs.URL_SUBMIT_NOTES,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {

                            try {
                                //converting response to json object
                                JSONObject obj = new JSONObject(response);

                                //if no error in response
                                Toast.makeText(getApplicationContext(), obj.getString("result"), Toast.LENGTH_SHORT).show();

                                //getting the user from the response
                                JSONObject userJson = obj.getJSONObject("result");

                                //starting the profile activity
                                finish();
                                startActivity(new Intent(getApplicationContext(), ProfileActivity.class));
                            } catch (JSONException e) {
                                e.printStackTrace();
                                System.out.printf("Error : " + e);
                            }
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            Toast.makeText(getApplicationContext(), error.getMessage(), Toast.LENGTH_SHORT).show();
                        }
                    }) {
                @Override
                protected Map<String, String> getParams() throws AuthFailureError {
                    Map<String, String> params = new HashMap<>();
                    params.put("log_content", editTextNotes.getText().toString().trim());
                    params.put("nim", nim);
                    return params;
                }
            };

            VolleySingleton.getInstance(this).addToRequestQueue(stringRequest);
        }
    }
}
