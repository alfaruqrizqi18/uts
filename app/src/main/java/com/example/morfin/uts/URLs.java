package com.example.morfin.uts;

/**
 * Created by Morfin on 30/10/2017.
 */

public class URLs {
//    private static final String ROOT_URL = "http://logbookapps.000webhostapp.com/";
    public static final String ROOT_URL = "http://192.168.200.222/logbook-api/";

    public static final String URL_REGISTER = ROOT_URL + "user/";
    public static final String URL_LOGIN= ROOT_URL + "auth/";
    public static final String URL_GET_LOG_BY_NIM= ROOT_URL + "log?nim=";
    public static final String URL_SUBMIT_NOTES= ROOT_URL + "log/";
    public static final String URL_DELETE_NOTES= ROOT_URL + "log/";
}
