package com.example.morfin.uts;

/**
 * Created by Morfin on 30/10/2017.
 */

public class User {
    public String owner, nim, password;

    public User( String owner, String nim, String password) {
        this.owner = owner;
        this.nim = nim;
        this.password = password;
    }


    public String getOwner() {
        return owner;
    }

    public String getNim() {
        return nim;
    }

    public String getPassword() {
        return password;
    }


}
